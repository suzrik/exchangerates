//
//  AppDelegate.h
//  ExchangeRates
//
//  Created by Dmitry Sizikov on 30.11.17.
//  Copyright © 2017 boardev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

