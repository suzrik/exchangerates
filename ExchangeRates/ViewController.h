//
//  ViewController.h
//  ExchangeRates
//
//  Created by Dmitry Sizikov on 30.11.17.
//  Copyright © 2017 boardev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *currencyTypeLabel;
@property (nonatomic, weak) IBOutlet UILabel *currencyValueLabel;
@property (nonatomic, weak) IBOutlet UILabel *currencyYesterdayLabel;
@property (nonatomic, weak) IBOutlet UILabel *lastUpdateLabel;

@property (nonatomic, weak) IBOutlet UIView *infoView;
@property (nonatomic, weak) IBOutlet UIView *menuView;
@property (nonatomic, weak) IBOutlet UIView *currencyListView;

- (IBAction) openCurrencyList;

@end

