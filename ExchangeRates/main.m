//
//  main.m
//  ExchangeRates
//
//  Created by Dmitry Sizikov on 30.11.17.
//  Copyright © 2017 boardev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
