//
//  ViewController.m
//  ExchangeRates
//
//  Created by Dmitry Sizikov on 30.11.17.
//  Copyright © 2017 boardev. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic, strong) NSArray *currencies;
@property (nonatomic, strong) UIAlertController *alert;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //единственое место где мы управляем количеством валют
    self.currencies = [self createAllPermutations:@[@"usd", @"rub", @"eur"]];
}

- (void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    //по умолчанию загружается первая комбинация валют
    [self selectCurrencyConvertation:[self.currencies firstObject]];
}


/**
 Обновление данных при выборе пары валют

 @param currencyConvertation NSDictionary
 */
- (void) selectCurrencyConvertation:(NSDictionary *) currencyConvertation {
    
    //ставим засветку
    [self showAlertWithTitle:NSLocalizedString(@"Loading alert title", nil) andDescription:NSLocalizedString(@"Loading alert description", nil) andShowCancel:NO];
    
    //меняем заголовок
    NSString *currencyTitle = [NSString stringWithFormat:@"%@ → %@", [[currencyConvertation objectForKey:@"from"] uppercaseString], [[currencyConvertation objectForKey:@"to"] uppercaseString]];
    
    NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:currencyTitle];
    [text addAttribute:NSKernAttributeName value:[NSNumber numberWithDouble:1] range:NSMakeRange(0, text.length)];
    [self.currencyTypeLabel setAttributedText:text];

    //загружаем данные с API
    [self loadCurrencyFromAPI:currencyConvertation];
}


/**
 Загрузка данных с API и обновление интерфейса

 @param currencyConvertation NSDictionary
 */
- (void) loadCurrencyFromAPI:(NSDictionary *) currencyConvertation {
    
    //загружаем текущий курс
    NSString *currentCurrencyUrl = [NSString stringWithFormat:@"https://api.fixer.io/latest?base=%@&symbols=%@", [[currencyConvertation objectForKey:@"from"] uppercaseString], [[currencyConvertation objectForKey:@"to"] uppercaseString]];

    NSURLRequest *currentCurrencyRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:currentCurrencyUrl]];
    
    [NSURLConnection sendAsynchronousRequest:currentCurrencyRequest
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
                               if (connectionError) {
                                   [self showAlertWithTitle:NSLocalizedString(@"Loading alert error title", nil) andDescription:NSLocalizedString(@"Loading alert error description", nil) andShowCancel:YES];

                                   return;
                               }
                               
                               NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                      options:0
                                                                        error:nil];
                               
                               //обновляем текущий курс в интерфейсе
                               NSString *keyPath = [NSString stringWithFormat:@"rates.%@", [[currencyConvertation objectForKey:@"to"] uppercaseString]];
                               NSNumber *currentCurrency = [json valueForKeyPath:keyPath];
                               
                               //округление до тысячных
                               NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                               [numberFormatter setRoundingMode: NSNumberFormatterRoundDown];
                               [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                               [numberFormatter setMaximumFractionDigits:3];
                               NSString *numberString = [numberFormatter stringFromNumber:currentCurrency];
                               
                               NSMutableAttributedString *text = [[NSMutableAttributedString alloc] initWithString:numberString];
                               
                               [text addAttribute:NSKernAttributeName value:[NSNumber numberWithDouble:-4] range:NSMakeRange(0, text.length)];
                               [self.currencyValueLabel setAttributedText:text];

                               //обновляем последнее время в интерфейсе
                               NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                               [formatter setDateFormat:@"HH:mm"];
                               NSString *lastUpdateDate = [formatter stringFromDate:[NSDate date]];
                               
                               NSMutableAttributedString *lastUpdatetext = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:NSLocalizedString(@"Last update at %@", nil), lastUpdateDate]];
                               
                               [lastUpdatetext addAttribute:NSKernAttributeName value:[NSNumber numberWithDouble:2] range:NSMakeRange(0, lastUpdatetext.length)];
                               [self.lastUpdateLabel setAttributedText:lastUpdatetext];

                               //главное - это текущий курс, поэтому показываем, что знаем сразу
                               self.lastUpdateLabel.hidden = NO;
                               self.currencyTypeLabel.hidden = NO;
                               self.currencyValueLabel.hidden = NO;
                               self.menuView.hidden = NO;
                               [self.alert dismissViewControllerAnimated:YES completion:nil];

                               //загружаем вчерашний курс
                               NSDate *yesterday = [[NSDate date] dateByAddingTimeInterval: -86400.0];
                               [formatter setDateFormat:@"yyyy-MM-dd"];
                               NSString *yesterdayString = [formatter stringFromDate:yesterday];
                               
                               NSString *yesterdayCurrencyUrl = [NSString stringWithFormat:@"https://api.fixer.io/%@?base=%@&symbols=%@", yesterdayString,  [[currencyConvertation objectForKey:@"from"] uppercaseString], [[currencyConvertation objectForKey:@"to"] uppercaseString]];
                               NSURLRequest *yesterdayCurrencyRequest = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:yesterdayCurrencyUrl]];
                               
                               [NSURLConnection sendAsynchronousRequest:yesterdayCurrencyRequest
                                                                  queue:[NSOperationQueue mainQueue]
                                                      completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                                                          NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data
                                                                                                               options:0
                                                                                                                 error:nil];
                                                          
                                                          if (connectionError) {
                                                              //мне кажется, что разница курсов валют не очень важная информация, в противном случае тут следует запустить таймер на повтор загрузки данных позже
                                                              return;
                                                          }
                                                          
                                                          NSString *keyPath = [NSString stringWithFormat:@"rates.%@", [[currencyConvertation objectForKey:@"to"] uppercaseString]];
                                                          NSNumber *yesterdayCurrency = [json valueForKeyPath:keyPath];
                                                          
                                                          //подсчитываем процент, оставил эти значения в виде отдельных переменных для удобства тестирования.
                                                          float today = [currentCurrency floatValue];
                                                          float yesterday = [yesterdayCurrency floatValue];
                                                          
                                                          float percent = 100-((today * 100) / yesterday);
                                                          
                                                          if (percent > 0) {
                                                              int percentCeil = (int)ceil(percent);
                                                              NSString *currencyLocalization = NSLocalizedString([currencyConvertation objectForKey:@"to"], nil);
                                                              self.currencyYesterdayLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Currency up", nil), currencyLocalization, percentCeil, [self percentPlural:percentCeil]];
                                                              [self.currencyYesterdayLabel setTextColor:[UIColor colorWithRed:.494 green:.827 blue:.129 alpha:1.0]];

                                                          }
                                                          else if (percent < 0) {
                                                              int percentCeil = (int)ceil(percent*-1);
                                                              NSString *currencyLocalization = NSLocalizedString([currencyConvertation objectForKey:@"to"], nil);
                                                              self.currencyYesterdayLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Currency down", nil), currencyLocalization, percentCeil, [self percentPlural:percentCeil]];
                                                              [self.currencyYesterdayLabel setTextColor:[UIColor colorWithRed:.816 green:.008 blue:.106 alpha:1.0]];

                                                          }
                                                          
                                                          else {
                                                              NSString *currencyLocalization = NSLocalizedString([currencyConvertation objectForKey:@"to"], nil);
                                                              self.currencyYesterdayLabel.text = [NSString stringWithFormat:NSLocalizedString(@"Currency stay", nil), currencyLocalization];
                                                              [self.currencyYesterdayLabel setTextColor:[UIColor colorWithRed:.494 green:.827 blue:.129 alpha:1.0]];
                                                          }

                                                          self.currencyYesterdayLabel.hidden = NO;
                                                          self.currencyYesterdayLabel.alpha = 0;
                                                          [UIView animateWithDuration:0.3 animations:^{
                                                              self.currencyYesterdayLabel.alpha = 1;
                                                          }];
                                                      }];

                           }];
}


/**
 Выбор правильной формы множественного числа

 @param percent int
 @return NSLocalizedString
 */
- (NSString *) percentPlural:(int) percent {
    if (percent%10 == 1 && percent != 11) {
        return NSLocalizedString(@"percent plural 1", nil);
    }
    else if ((percent%10 > 1 && percent%10 <= 4) && (percent < 11 || percent > 14)) {
        return NSLocalizedString(@"percent plural 2", nil);
    }
    else {
        return NSLocalizedString(@"percent plural 3", nil);
    }
}


/**
 Алерт о загрузке курса или ошибке

 @param title NSString
 @param description NSString
 @param cancelButton BOOL
 */
- (void) showAlertWithTitle:(NSString *) title andDescription:(NSString *) description andShowCancel:(BOOL) cancelButton {
    if (self.alert) {
        [self.alert dismissViewControllerAnimated:YES completion:nil];
    }
    
    self.alert = [UIAlertController alertControllerWithTitle:
                  title message:description preferredStyle:UIAlertControllerStyleAlert];
    if (cancelButton) {
        [self.alert addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil) style: UIAlertActionStyleCancel handler:nil]];
    }

    self.alert.popoverPresentationController.barButtonItem = self.navigationItem.rightBarButtonItem;
    self.alert.popoverPresentationController.permittedArrowDirections = UIPopoverArrowDirectionUp;
    
    self.alert.popoverPresentationController.sourceView = self.view;
    self.alert.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width / 2.0, self.view.bounds.size.height / 2.0, 1.0, 1.0);
    
    [self presentViewController:self.alert animated:YES completion:nil];
}

/**
 Перебираем все варианты конвертаций

 @param currencies NSArray возможные валюты
 @return NSArray все варианты конвертаций
 */
- (NSArray *) createAllPermutations:(NSArray *) currencies {
    NSMutableArray *allPermutationsArray = [[NSMutableArray alloc] init];
    for (NSString *currencyParrent in currencies) {
        for (NSString *currencyChild in currencies) {
            if (![currencyParrent isEqualToString:currencyChild]) {
                [allPermutationsArray addObject:@{@"from": currencyParrent, @"to": currencyChild}];
            }
        }
    }

    return allPermutationsArray;
}


/**
 Открываем меню с выбором валют
 */
- (IBAction) openCurrencyList {
    
    NSInteger cellHeight = 44;

    //TODO: для демонстрации своих навыков эта часть симпатичнее выглядела бы в виде autolayout,
    //но из коротких сроков и простой задачи я решил не мудрить.
    for (NSUInteger counter = 0; counter < [self.currencies count]; counter++) {
        NSDictionary *currencyConvertation = [self.currencies objectAtIndex:counter];
        NSString *currencyTitle = [NSString stringWithFormat:@"%@ → %@", [[currencyConvertation objectForKey:@"from"] uppercaseString], [[currencyConvertation objectForKey:@"to"] uppercaseString]];

        //+counter - это полосочка между "ячейками", делать в этом месте таблицы - это переусложнять задачу.
        UIButton *currencyButton = [[UIButton alloc] initWithFrame:CGRectMake(0, (counter*cellHeight) + counter, [[UIScreen mainScreen] bounds].size.width, cellHeight)];
        [currencyButton setTitle:currencyTitle forState:UIControlStateNormal];
        
        //текущая выбранная валюта подсвечивается жирным шрифтом.
        if ([self.currencyTypeLabel.attributedText.string isEqualToString:currencyTitle]) {
            currencyButton.titleLabel.font = [UIFont fontWithName:@"Lato-Bold" size:17.0];
        }
        else {
            currencyButton.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:17.0];
        }
        
        [currencyButton setBackgroundColor:[UIColor colorWithRed:.247 green:.278 blue:.325 alpha:1.0]];
        currencyButton.tag = counter;
        [currencyButton addTarget:self action:@selector(selectCurrency:) forControlEvents:UIControlEventTouchUpInside];
        [self.currencyListView addSubview:currencyButton];
    }

    NSInteger currencyListViewHeight = (cellHeight * [self.currencies count]) + [self.currencies count];
    
    //анимация открытия
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.currencyListView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height - currencyListViewHeight, [[UIScreen mainScreen] bounds].size.width, currencyListViewHeight);
                     }
                     completion:nil];
    
    //анимация сдвига UIView с валютами, актуально для маленьких телефонов
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.infoView.frame = CGRectOffset(self.infoView.frame, 0, -64);
                     }
                     completion:nil];
}


/**
 Действие на выбор валюты из меню

 @param sender UIButton
 */
- (void) selectCurrency:(UIButton *) sender {

    //анимация закрытия
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.currencyListView.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, [[UIScreen mainScreen] bounds].size.width,0);
                     }
                     completion:^(BOOL finished){
                         [self.currencyListView.subviews makeObjectsPerformSelector: @selector(removeFromSuperview)];
                     }];

    //анимация сдвига UIView с валютами, актуально для маленьких телефонов
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.infoView.frame = CGRectOffset(self.infoView.frame, 0, 64);
                     }
                     completion:nil];

    //обновляем данные
    NSDictionary *currencyConvertation = [self.currencies objectAtIndex:sender.tag];
    [self selectCurrencyConvertation:currencyConvertation];
}

@end
